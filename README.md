# Personal configuration files

This repository contains my personal configuration files

There are various dotfiles for different window manager and usual CLI
applications, as well as useful scripts.

There are also my Nix configuration files.
