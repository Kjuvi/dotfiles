{ config, pkgs, ...}:


{
  programs.zsh = {
    enable = true;
    autosuggestions.enable = true;
    syntaxHighlighting.enable = true;

    ohMyZsh.enable = true;
    ohMyZsh.plugins = [ "fzf" "sudo" ];
    ohMyZsh.theme = "agnoster";

    interactiveShellInit=''
      eval "$(starship init zsh)"

      PATH=$PATH:$HOME/.scripts
      PATH=$PATH:$HOME/.npm-global/bin
    '';
  };

  environment.systemPackages = with pkgs; [
    any-nix-shell
    nix-zsh-completions
    starship
  ];
}
