{ config, pkgs, ...}:

{
  services.xserver = {
    desktopManager.plasma5.enable = true;
    displayManager.sddm.enable = true;
  };

  environment.systemPackages = with pkgs; [

    # Development
    kate
    qtcreator

    # Internet
    ktorrent

    # Multimedia
    okular
    vlc

    # Office
    ark

    # System
    yakuake
  ];
}
