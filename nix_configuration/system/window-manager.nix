{ config, pkgs, ...}:

{
  services.xserver = {
    # TODO: enable a WM here  
    # windowManager.XXX.enable = true;
  };

  environment.systemPackages = with pkgs; [

    # Multimedia
    feh
    celluloid
    maim

    # Office
    zathura

    # System
    compton-git
    gnome3.adwaita-icon-theme
    killall
    kitty
    lxappearance-gtk3
    ncpamixer
    networkmanagerapplet
    (polybar.override { pulseSupport = true; })
    qt5ct
    ranger
    rofi
    sxhkd
    xorg.xbacklight
    xorg.xf86inputlibinput
  ];

  environment.variables.QT_QPA_PLATFORMTHEME = "qt5ct";
}
