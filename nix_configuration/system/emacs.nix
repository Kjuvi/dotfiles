{ pkgs ? import <nixpkgs> {} }:

let
  myEmacs = pkgs.emacs;
  emacsWithPackages = (pkgs.emacsPackagesGen myEmacs).emacsWithPackages;
in
  emacsWithPackages (epkgs: (with epkgs.melpaPackages; [
    # all-the-icons
    centaur-tabs
    company
    counsel
    company-lsp
    flycheck
    flx-ido
    go-mode
    ivy
    # lsp-ivy
    lsp-mode
    lsp-python-ms
    lsp-ui
    magit
    nix-mode
    projectile
    swiper
    treemacs
    treemacs-projectile
    use-package
    yasnippet
    yasnippet-snippets
    zenburn-theme
  ]) ++ [
    pkgs.python37Packages.python-language-server
  ])
