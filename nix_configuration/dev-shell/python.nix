with import <nixpkgs> {};

pkgs.mkShell rec {

  name = "python-shell";

  buildInputs = with pkgs; [
    # Editor dependencies (auto-completion, syntax checking, ...)
    autoflake
    python37Packages.flake8
    python37Packages.python-language-server
    python37Packages.yapf

    # Project dependencies
  ];

}

