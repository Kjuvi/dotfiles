with import <nixpkgs> {};

pkgs.mkShell rec {

  name = "javascript-shell";
  
  buildInputs = with pkgs; [
    # Editor dependencies (auto-completion, syntax checking, ...)
    nodePackages.eslint
    nodePackages.typescript-language-server

    # Project dependencies
  ];

}
