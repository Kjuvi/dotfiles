with import <nixpkgs> {};

pkgs.mkShell rec {

  name = "qtcreator-shell";

  buildInputs = with pkgs; [
    # Editor dependencies (auto-completion, syntax checking, ...)
    cmake
    gcc
    gdb
    pkg-config
    qt5.full
    qtcreator

    # Project dependencies
  ];

}
