with import <nixpkgs> {};

pkgs.mkShell rec {

  name = "cpp-shell";

  buildInputs = with pkgs; [
    # Editor dependencies (auto-completion, syntax checking, ...)
    ccls

    # Project dependencies
  ];

}
