;;; packages.el --- packages configuration
;;;
;;; Commentary:
;;; Configuration of all the packages which are loaded are contained in this file.
;;;
;;; Code:

(use-package all-the-icons
  :ensure t)


(use-package company
  :ensure t
  :init (setq company-minimum-prefix-length 1
              company-idle-delay 0.0)
  :hook (after-init . global-company-mode))


(use-package counsel
  :ensure t)


(use-package dockerfile-mode
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode)))


(use-package doom-modeline
  :ensure t
  :hook (after-init . doom-modeline-mode))


;; (use-package evil
;;   :init (setq evil-want-keybinding nil)
;;   :config
;;   (evil-mode 1)
;;   (evil-global-set-key 'normal (kbd "C-n") 'treemacs))


;; (use-package evil-collection
;;   :after evil
;;   :config (evil-collection-init))


(use-package flycheck
  :ensure t
  :hook (after-init . global-flycheck-mode))


(use-package go-mode
  :ensure t)


(use-package gruvbox-theme
  :ensure t
  :config (load-theme 'gruvbox t))


(use-package ivy
  :ensure t
  :init
  (setq ivy-use-virtual-buffers t
        enable-recursive-minibuffers t
        search-default-mode #'char-fold-to-regexp
        ivy-re-builders-alist
        '((t . ivy--regex-fuzzy)))
  :config
  (ivy-mode 1)
  (counsel-mode 1)
  :bind
  ("C-s" . 'counsel-rg)
  ("M-s" . 'counsel-fzf))


(use-package lsp-mode
  :ensure t
  :init (setq lsp-keymap-prefix "M-l"
              lsp-prefer-capf t)
  :hook
  (go-mode . lsp-deferred)
  (javascript-mode . lsp-deferred)
  (python-mode . lsp-deferred)
  (terraform-mode . lsp-deferred)
  :commands (lsp lsp-deferred))


(use-package lsp-ivy
  :ensure t
  :commands lsp-ivy-workspace-symbol)


(use-package lsp-ui
  :ensure t
  :hook (lsp-mode . lsp-ui-mode)
  :commands lsp-ui-mode)


(use-package lsp-treemacs
  :ensure t
  :commands lsp-treemacs-errors-list)


(use-package nix-mode
  :ensure t
  :mode "\\.nix\\'")


;; (use-package projectile
;;   :ensure t
;;   :config
;;   (define-key projectile-mode-map (kbd "M-p") 'projectile-command-map)
;;   (projectile-mode 1)
;;   (setq projectile-completion-system 'ivy))


(use-package swiper
  :ensure t)


(use-package terraform-mode
  :ensure t)


(use-package treemacs
  :ensure t
  :config
  (define-key treemacs-mode-map [mouse-1] #'treemacs-single-click-expand-action)
  (progn
    (setq treemacs-show-hidden-files nil))
  :bind ("C-c n" . 'treemacs))


(use-package which-key
  :ensure t
  :config (which-key-mode))


;; (use-package treemacs-evil
;;   :after treemacs evil)


;(use-package yasnippet
  ;:config (yas-reload-all)
  ;:hook (prog-mode . yas-minor-mode))

;;; packages.el ends here
