;;; config.el --- global configuraiton
;;;
;;; Commentary:
;;; gloabl configuration such as modes, keybindings, variables, etc...
;;;
;;; Code:

      ;; No splash screen.
(setq inhibit-startup-screen t
      inhibit-scratch-message nil
      ;; No auto-sav, lock and backup files.
      auto-save-default nil
      create-lockfiles nil
      make-backup-files nil
      ;; Smooth scrolling.
      mouse-wheel-scroll-amount '(3 ((shift) . 3)) ;; mouse scroll 3 lines at a time
      mouse-wheel-progressive-speed nil
      mouse-wheel-follow-mouse 't
      ;; no tool bar
      scroll-ster 1 ;; keyboard scroll 1 line at a time
      scroll-error-top-bottop t
      ;; One space for sentence separation.
      sentence-end-double-space nil
      ;; Parenthesis
      show-paren-delay 0
      ;; No anoying bell
      visible-bell 1)


;; hide the menu and toolbar
(menu-bar-mode -1)
(tool-bar-mode -1)

;; Start emacs fullscreen
(custom-set-variables
 '(initial-frame-alist (quote ((fullscreen . maximized)))))


;; buffer local variables.
;; Indent with 4 spaces when pressing TAB key.
(setq-default c-basic-offset 4
              fill-column 80
              indent-tabs-mode nil
              tab-width 4)


;; Accept 'y' and 'n' rather than 'yes' and 'no'.
(defalias 'yes-or-no-p 'y-or-n-p)


;; modes
(setq auto-fill-mode 1
      column-number-mode 1)

(electric-indent-mode 1)
(electric-pair-mode 1)
(global-display-line-numbers-mode 1)
(scroll-bar-mode 0)
(show-paren-mode 1)
(global-hl-line-mode 1)


;; global keybindings
(global-set-key (kbd "<C-up>") 'shrink-window)
(global-set-key (kbd "<C-down>") 'enlarge-window)
(global-set-key (kbd "<C-left>") 'shrink-window-horizontally)
(global-set-key (kbd "<C-right>") 'enlarge-window-horizontally)


;; font configuration
(set-frame-font "FiraCode Nerd Font 12")

;;; config.el ends here

