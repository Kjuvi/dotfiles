#+LATEX_HEADER: \newcommand{\HRule}{\rule{\linewidth}{0.5mm}}
#+BEGIN_titlepage
  \center
   
  \textsf{\Large {{{institution}}}}
  \vspace{1.5cm}

  \textsf{\Large {{{major_heading}}}}
  \vspace{0.5cm}

  \textsf{\Large {{{minor_heading}}}}
  \vspace{0.5cm}

  \HRule\vspace{0.4cm}
    \huge \textsf{ \bfseries {{{title}}}}
  \vspace{0.4cm}\HRule\vspace{1.5cm}

  \large Author: \hfill Supervisor:
  \vskip 1em
  \large {{{author}}} \hfill {{{supervisor}}}

  \vfill

  \large {{{date}}}
  
  \vfill

  \includegraphics[scale={{{logo_scale}}}]{{{{logo}}}}

  \vfill
#+END_titlepage
