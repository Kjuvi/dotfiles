source $HOME/.config/nvim/config/plugins.vim
source $HOME/.config/nvim/config/general.vim
source $HOME/.config/nvim/config/ui.vim
source $HOME/.config/nvim/config/keys.vim
source $HOME/.config/nvim/config/ale.vim
"source $HOME/.config/nvim/config/asyncomplete.vim
" source $HOME/.config/nvim/config/lsp.vim
" source $HOME/.config/nvim/config/coc.vim

