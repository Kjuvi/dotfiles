#!/bin/sh

ACTIONS=("shutdown" "suspend" "reboot")


# List the possible actions
rofi_list() {
	for action in ${ACTIONS[*]}; do
		echo "$action"
	done
}


main() {
	action=$( (rofi_list) | rofi -dmenu -p "" )

	if [ "$action" == "suspend" ]; then
		dm-tool lock; systemctl suspend
	elif [ "$action" == "shutdown" ]; then
		shutdown now
	elif [ "$action" == "reboot" ]; then
		reboot
	fi
}

main

exit 0