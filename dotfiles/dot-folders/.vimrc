"Plug.vim automatic install
set autoindent
set encoding=utf-8
set expandtab
set path+=**
set wildmenu

set foldlevel=99
set foldmethod=indent

set mouse=a
set number

set shiftwidth=4
set showmatch
set softtabstop=4
set tabstop=4
set textwidth=79

set splitright
set splitbelow

set spell spelllang=en_us

syntax enable
colorscheme bubblegum-256-dark


let mapleader="\<space>"

" Move between vsplit and resize them
map <C-H> <C-W>h
map <C-L> <C-W>l
map + 5<C-W><
map - 5<C-W>>

map <C-T> :tabnew<CR>
map <C-Tab> :tabnext<CR>

map <Leader>n :NERDTreeToggle<CR>
nmap <Leader><Enter> O<esc>j

let g:airline_theme='bubblegum'

if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source ~/.vimrc
endif

call plug#begin('~/.vim/plugged')

Plug 'https://github.com/Valloric/YouCompleteMe'

Plug 'scrooloose/nerdtree'

Plug 'vim-airline/vim-airline'

Plug 'vim-airline/vim-airline-themes'

call plug#end()

