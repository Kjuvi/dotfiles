#!/run/current-system/sw/bin/python
"""
Simple utility to write the brightness value into the related /sys/... file.
"""

import sys

# configuration
MIN_BRIGHTNESS = 100


def read_max_brightness():
    """Read the maximum brightness value written into /sys/..."""
    with open('/sys/class/backlight/intel_backlight/max_brightness', 'r') as f:
        max_brightness = int(f.read())
    return max_brightness


def increment(percentage):
    """Increment backlight by the given percentage."""
    max_brightness = read_max_brightness()
    print(max_brightness)
    with open('/sys/class/backlight/intel_backlight/brightness', "r+") as f:
        brightness = int(f.read())
        brightness += int(max_brightness * percentage)
        print(brightness)
        if brightness > max_brightness:
            brightness = max_brightness
        brightness = str(brightness)
        f.write(brightness)


def decrement(percentage):
    """Decrement backlight by the given percentage."""
    max_brightness = read_max_brightness()
    with open("/sys/class/backlight/intel_backlight/brightness", "r+") as f:
        brightness = int(f.read())
        brightness -= int(max_brightness * percentage)
        if brightness < MIN_BRIGHTNESS:
            brightness = MIN_BRIGHTNESS
        brightness = str(brightness)
        f.write(brightness)


def help():
    print("Usage: backlight [OPTION] [PERCENTAGE]")
    print("Increase or decrease the backlight by a given PERCENTAGE")
    print()
    print("  -i [PERCENTAGE]           increment backlight by PERCENTAGE")
    print("  -d [PERCENTAGE]           decrement backlight by PERCENTAGE\n")

if __name__ == '__main__':
    try:
        action = sys.argv[1]
        percentage = int(sys.argv[2]) / 100

        if percentage > 0 and percentage < 1:
            if action == '-i': 
                increment(percentage)
            elif action == '-d':
                decrement(percentage)
            else:
                raise Exception
        else: 
            raise Exception
    except Exception as e:
        help()
