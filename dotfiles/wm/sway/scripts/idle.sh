#!/bin/bash

exec swayidle -w \
    timeout 300 'swaylock -i /usr/share/wallpapers/openSUSEdefault/contents/images/1920x1080.jpg -s fill' \
    timeout 600 'swaymsg "output * dpms off"' \
    resume 'swaymsg "output * dpms on"'
