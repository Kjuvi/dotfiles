#!/bin/sh

ACTIONS=("shutdown" "reboot")

# List the possible actions
wofi_list() {
	for action in ${ACTIONS[*]}; do
		echo "$action"
	done
}


main() {
	action=$( (wofi_list) | /opt/wofi/build/wofi --show dmenu -p )

    if [ "$action" == "shutdown" ]; then
        shutdown now
    elif [ "$action" == "reboot" ]; then
        reboot
    fi
}

main

exit 0
