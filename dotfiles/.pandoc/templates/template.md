---
language: english
margin: 2.5cm
titlepage: true
institution: Institution
major_heading: Major heading
minor_heading: Minor heading
title: Title
subtitle: Subtitle
authors: [Quentin Vaucher, Another Author]
supervisors: [Supervisors I, Sup II]
logo: logo.jpg
date: \today
abstract: Abstract...
toc: true
lof: true
lot: true
lhead: Left header
rhead: Right header
...

## Introduction

blabla

### sub intro

blabla sub intro bla here...

