# Configuration variables

the values shown is the default value of each variable if they are not not present

- language: english
- lhead: 
- rhead: 
- titlepage: false
- institution: 
- major_heading: 
- minor_heading: 
- title: 
- subtitle:
- authors: []
- supervisors: []
- logo: 
- date: \today
- abstract: 
- toc: false
- lof: false
- lot: false
- margin: 2.5cm
